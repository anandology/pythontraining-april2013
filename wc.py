import sys

def linecount(filename):
    return len(open(filename).readlines())

def wordcount(filename):
    return len(open(filename).read().split())

def charcount(filename):
    return len(open(filename).read())

def main():
    fn = sys.argv[1]
    print linecount(fn), wordcount(fn), charcount(fn), fn

main()

