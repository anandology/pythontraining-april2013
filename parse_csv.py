
def parse_csv(filename):
    return [line.strip().split(",") for line in open(filename).readlines() if not line.startswith("#")]

print parse_csv("a.csv")
