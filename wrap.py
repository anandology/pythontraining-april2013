import sys

def wrapline(line, width):
    """Splits the line in to parts, so that
    each part is at max width characters long.
        
        >>> wrapline('helloworld', 5)
        ['hello', 'world']
        >>> wrapline('helloworld', 8)
        ['hellowor', 'ld']
    """
    parts = []
    for i in range(0, len(line), width):
        part = line[i:i+width]
        parts.append(part)
    return parts
    return [line[i:i+width] for i in range(0, len(line), width)]

def wrapfile(filename, width):
    """Wraps each line in the file if it is 
    longer than width characters and prints 
    the output.
    """
    for line in open(filename).readlines():
        for part in wrapline(line.strip("\n"), width):
            print part

def main():
    filename = sys.argv[1]
    width = int(sys.argv[2])
    wrapfile(filename, width)

main()
