import urllib
import sys

def download(url):
    return urllib.urlopen(url).read()

def get_filename(url):
    if url.endswith("/"):
        return "index.html"
    else:   
        return url.split("/")[-1]

def main():
    url = sys.argv[1]
    content = download(url)
    filename = get_filename(url)
    
    print "saving", url, "as", filename
    f = open(filename, "w")
    f.write(content)
    f.close()
    
main()
