
# counts number of occurances of
# s2 in s1, ignoring the case.
def icount(s1, s2):
    s1 = s1.lower()
    s2 = s2.lower()
    return s1.count(s2)

print icount("helloworld", "o")

print icount("HELLOworld", "o")

print icount("HELLOworld", "ow")

