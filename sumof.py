
def square(x):
    return x*x

def cube(x):
    return x*x*x

def sum_of_squares(x, y):
    return square(x) + square(y)

def sum_of_cubes(x, y):
    return cube(x) + cube(y)

def sumof(f, x, y):
    return f(x) + f(y)


