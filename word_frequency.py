import sys

def read_words(filename):
    return open(filename).read().split()

def print_frequency(freq):
    print freq

def word_frequency(words):
    freq = {}
    for w in words:
        freq[w] = freq.get(w, 0) + 1
    return freq

def main(filename):
    words = read_words(filename)
    freq = word_frequency(words)
    print_frequency(freq)

main(sys.argv[1])
