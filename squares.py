def square(x):
    return x * x

def cube(x):
    return x*x*x

def squares(values):
    result = []
    for v in values:
        result.append(square(v))
    return result

def cubes(values):
    result = []
    for v in values:
        result.append(cube(v))
    return result

def make_list(f, values):
    result = []
    for v in values:
        result.append(f(v))
    return result

print squares([1, 2, 3, 4])
print make_list(square, [1, 2, 3, 4])
print make_list(cube, [1, 2, 3, 4])


