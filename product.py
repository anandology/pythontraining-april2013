
def product(values):
    result = 1
    for v in values:
        result = result * v
    return result

print product([1, 2, 3, 4])

def factorial(n):
    return product(range(1, n+1))

print factorial(10)
print factorial(100)

