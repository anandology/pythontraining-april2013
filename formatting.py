import sys

def format_file(filename, formatter):
    text = open(filename).read()
    return formatter.format(text)

class UpperCaseFormatter:
    def format(self, text):
        return text.upper()

class LineFormatter:
    def format(self, text):
        lines = text.split("\n")
        lines = [self.format_line(line) for line in lines]
        return "\n".join(lines)

    def format_line(self, line):
        """The base classes should override this function to format a single line.
        """
        raise NotImplementedError()

class CenterAlignFormatter(LineFormatter):
    def __init__(self, width):
        self.width = width

    def format_line(self, line):
        return line.center(self.width)

class CompoundFormatter:
    def __init__(self, formatters):
        self.formatters = formatters

    def format(self, text):
        for f in self.formatters:
            text = f.format(text)
        return text

class WrapFormatter:
    def __init__(self, width):
        self.width = width

    def format(self, text):
        lines = text.split("\n")

        wrapped_lines = [part for line in lines for part in self.wrapline(line)]

        # wrapped_lines = []
        # for line in lines:
        #     for part in self.wrapline(line):
        #         wrapped_lines.append(part)

        return "\n".join(wrapped_lines)
        
    def wrapline(self, line):
        return [line[i:i+self.width] for i in range(0, len(line), self.width)]

def main():
    filename = sys.argv[1]
    formatters = [
        UpperCaseFormatter(),
        CenterAlignFormatter(40)]

    f = CompoundFormatter(formatters)
    print format_file(filename, f)

if __name__ == "__main__":
    main()

