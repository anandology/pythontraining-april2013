
def minimum(x, y):
    ...

print minimum(3, 4)
print minimum(9, 3)

def minimum3(x, y, z):
    ...

print minimum3(2, 3, 4)
print minimum3(12, 3, 14)
print minimum3(12, 13, 4)

