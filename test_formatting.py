import unittest
from formatting import UpperCaseFormatter

class TestUpperCaseFormatter(unittest.TestCase):
    def test_upper(self):
        f = UpperCaseFormatter()
        self.assertEqual(f.format("hello"), "HELLO")

if __name__ == "__main__":
    unittest.main()
