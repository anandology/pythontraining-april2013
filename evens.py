
def is_even(n):
    return n % 2 == 0

def evens(values):
    ...

print evens([1, 2, 3, 4, 5, 6])
# [2, 4, 6]
