"""Implementation of built-in zip function.

Assuming that both lists x and y are of same length.
"""

def myzip1(x, y):
    result = []
    for i in range(len(x)):
        result.append((x[i], y[i]))
    return result

def myzip2(x, y):
    return [(x[i], y[i]) for i in range(len(x))]


x = [1, 2, 3, 4]
y = [2, 3, 4, 5]

print zip(x, y)
print myzip1(x, y)
print myzip2(x, y)
