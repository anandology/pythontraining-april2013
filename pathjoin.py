
def pathjoin(parts):
    ...

print pathjoin(["a", "b", "c.txt"])
# a/b/c.txt
    
print pathjoin(["a/b/", "c.txt"])
# a/b/c.txt
